#!/bin/bash
source .env
source $HOME/.bash_aliases

NOMBRE_LAMBDA=$(echo "$NOMBRE" | tr -d '\r')
RED_LAMBDA=$(echo "$RED" | tr -d '\r')
PERFIL_AWS=$(echo "$PROFILE_DEV" | tr -d '\r')

#Busqueda de puerto
BASE=3000
INCREMENT=1

port=$BASE
isfree=$(netstat -tapln | grep $port)

while [[ -n "$isfree" ]]; do
  port=$[port+INCREMENT]
  isfree=$(netstat -tapln | grep $port)
done

docker build -t $NOMBRE_LAMBDA .
shopt -s expand_aliases
DIR=${PWD}

aper --renew $PERFIL_AWS --output "$DIR/.env"

echo "###########################################################################################"
echo "Lambda deployando en http://localhost:$port/2015-03-31/functions/function/invocations"
echo "Probar con curl -XPOST http://localhost:$port/2015-03-31/functions/function/invocations -d \"{}\""
echo "###########################################################################################"

docker run -p $port:8080  --network $RED_LAMBDA --env-file "$DIR/.env"  $NOMBRE_LAMBDA
