#!/bin/bash

ARG1=${1:-0}
source .env
source $HOME/.bash_aliases

NOMBRE_MS=$(echo "$PROYECTO" | tr -d '\r')
PERFIL_AWS=VALYRIO-Developer
RED_MS=$(echo "$RED" | tr -d '\r')

#Busqueda de puerto
BASE=3000
INCREMENT=1

port=$BASE
isfree=$(netstat -tapln | grep $port)

while [[ -n "$isfree" ]]; do
  port=$[port+INCREMENT]
  isfree=$(netstat -tapln | grep $port)
done

docker build -t $NOMBRE_MS .
shopt -s expand_aliases
DIR=${PWD}

if [ $ARG1 == "--vola-db" ]; then
docker run -t --env-file "$DIR/.env" $NOMBRE_MS sh -c "node db/dynamo_migrate.js"
echo "###########################################################################################"
echo "DB Dynamo limpia"
echo "###########################################################################################"
fi

aper --renew $PERFIL_AWS --output "$DIR/.env"

echo "###########################################################################################"
echo "MS deployando en http://localhost:$port"
echo "###########################################################################################"

docker run -t -p $port:3000 --network $RED_MS  --env-file "$DIR/.env" $NOMBRE_MS