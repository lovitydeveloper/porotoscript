const AWS = require('aws-sdk');
const fs = require('fs');

const config = {
    region: process.env.AWS_REGION,
    endpoint: process.env.DYNAMODB_ENDPOINT,
    apiVersion: 'latest',
    credentials: {
        accessKeyId: 'asfasf',
        secretAccessKey: 'asasfsf'
    }
}

const ddb = new AWS.DynamoDB(new AWS.Config(config));

const tablas = ["useractions"];

tablas.forEach(t => {
    ddb.deleteTable({TableName: t}).promise().then(function(){
        let rawdata = fs.readFileSync(__dirname+'/'+t+'.json');
        let tabla = JSON.parse(rawdata);

        ddb.createTable(tabla, function (err, data) {
            if (err) {
                console.log("Error", err);
            } else {
                console.log("Table Created", data);
            }
        });
    })

})
