<?php

$autoloadPathAperSearch = _PS_MODULE_DIR_ . 'apersearchcloud/vendor/autoload.php';
if (file_exists($autoloadPathAperSearch)) {
    require_once $autoloadPathAperSearch;
}

$autoloadAwsSdk = _PS_MODULE_DIR_ . 'apersearchcloud/SDK/vendor/autoload.php';
if (file_exists($autoloadAwsSdk)) {
    require_once $autoloadAwsSdk;
}
