#!/bin/bash
#cp /extras/galicia/api_suppliers.php /var/www/galicia/config/
cp /extras/galicia/parameters.php /var/www/galicia/app/config/
#cp /extras/galicia/settings_custom.inc.php /var/www/galicia/config/

if [ ! -d "/var/www/galicia/translations" ] 
then
    mkdir /var/www/galicia/translations
fi
if [ ! -d "/var/www/galicia/translations/cldr" ] 
then
    mkdir /var/www/galicia/translations/cldr
fi

cd /var/www/galicia && cd /var/www/valyrio && for dir in $(find .  -name "composer.json" |xargs dirname);do ( cd $dir && echo "$dir [$(composer install)]") ; done

chmod 777 -R /var/www/galicia/