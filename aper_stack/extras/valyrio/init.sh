#!/bin/bash
cp /extras/valyrio/api_suppliers.php /var/www/valyrio/config/
cp /extras/valyrio/parameters.php /var/www/valyrio/app/config/
cp /extras/valyrio/settings_custom.inc.php /var/www/valyrio/config/
cp /extras/valyrio/defines-custom.inc.php /var/www/valyrio/config/

if [ ! -d "/var/www/valyrio/translations" ] 
then
    mkdir /var/www/valyrio/translations
fi
if [ ! -d "/var/www/valyrio/translations/cldr" ] 
then
    mkdir /var/www/valyrio/translations/cldr
fi

cd /var/www/valyrio && composer update && for dir in $(find .  -name "composer.json" -not -path "*/vendor/*" |xargs dirname);do ( cd $dir && echo "$dir [$(composer install)]") ; done

chmod 777 -R /var/www/valyrio/
