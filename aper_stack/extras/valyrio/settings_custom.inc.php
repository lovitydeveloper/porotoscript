<?php

require_once('api_suppliers.php');

$autoloadPathAperStore = _PS_MODULE_DIR_ . 'aperstore/vendor/autoload.php';
if (file_exists($autoloadPathAperStore)) {
    require_once $autoloadPathAperStore;
}

/*
$autoloadPathCsrf = _PS_MODULE_DIR_ . 'aperstore/src/Classes/csrf/vendor/autoload.php';
if (file_exists($autoloadPathCsrf)) {
    require_once $autoloadPathCsrf;
}
*/

$autoloadPathAperSearch = _PS_MODULE_DIR_ . 'apersearchcloud/vendor/autoload.php';
if (file_exists($autoloadPathAperSearch)) {
    require_once $autoloadPathAperSearch;
}

$autoloadAwsSdk = _PS_MODULE_DIR_ . 'apersearchcloud/SDK/vendor/autoload.php';
if (file_exists($autoloadAwsSdk)) {
    require_once $autoloadAwsSdk;
}

