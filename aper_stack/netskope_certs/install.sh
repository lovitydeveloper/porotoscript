#!/bin/bash
sudo cp aper_stack/netskope_certs/bundle-netskope-ca.crt /usr/local/share/ca-certificates/
sudo update-ca-certificates
git config --global http.sslCAInfo /usr/local/share/ca-certificates/bundle-netskope-ca.crt
git config --global http.sslCAPath /usr/local/share/ca-certificates/


for file in $(find ${HOME}/.local /usr/local -type f  -path '*botocore/cacert.pem' 2>/dev/null)
do
    echo "#START OF NETSKOPE CERTIFICATES" | sudo tee -a $file
    cat /usr/local/share/ca-certificates/bundle-netskope-ca.crt | sudo tee -a $file
    echo "#END OF NETSKOPE CERTIFICATES" | sudo tee -a $file
done
