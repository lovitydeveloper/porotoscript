import aws
import util
import globals
import time

def menuDbs():
    arrayArn = [obj["perfil"] for obj in globals.dbs] 
    arrayArn = list(dict.fromkeys(arrayArn))
    while True:
        choice  = util.menuMaker(arrayArn,'DBS')
        choice = choice.lower()
        if choice == '0':
            return
        if 1 <= int(choice) <= len(arrayArn):
            while True:
                arn = arrayArn[int(choice)-1]
                arrayAuxDbs = [obj for obj in globals.dbs if obj["perfil"]==arn]
                arrayDbs = [obj["nombre"] for obj in globals.dbs if obj["perfil"]==arn]
                
                choice  = util.menuMaker(arrayDbs,arn)
                choice = choice.lower()

                if choice == '0':
                    break
                else:
                    idxDb = int(choice)-1
                    print('Ingrese numero de puerto LOCAL :')
                    inp_puerto = input()

                    aux = sorted([{"perfil":obj.lstrip('profile').replace(" ", ""),"nombre":globals.aws_config[obj]["nombre"],"rol": globals.aws_config[obj]["rol"] if "rol" in dict(globals.aws_config[obj]).keys() is not None else globals.aws_config[obj]["sso_role_name"],"auth_aws":globals.aws_config[obj]["auth_aws"] } for obj in globals.aws_config],key=lambda i: i["perfil"])
                    
                    perfiles = []
                    for a in aux:
                        if (a['nombre'].lower() == arn.lower()):
                            perfiles.append(a)
                    if len(perfiles) == 0:
                        print('No Existen Perfiles con este nombre')
                        return 
                        input()
                    elif len(perfiles) == 1:
                        perfil = perfiles[0]
                    else:
                        i = 1
                        for p in perfiles:
                            print (str(i)+" : "+p["perfil"])
                            i = i+1
                        
                        inpPerfil = input("Seleccione Perfil que desea usar: ")
                        idxPerfil = int(inpPerfil)-1
                        perfil = perfiles[idxPerfil]

                    if (perfil["auth_aws"].lower() == 'sso'):
                        aws.authSSO(perfil["perfil"])
                        
                    else:
                        token_temp = aws.assumeRole(perfil)
                        aws.setOsHostCredentials(token_temp,perfil["perfil"])

                    aws.setOsSshConf(perfil)
                    cmds = []
                    user_ssh = 'db-user'

                    cmds.append('ssh -L*:'+inp_puerto+':'+arrayAuxDbs[idxDb]["url"]+':'+arrayAuxDbs[idxDb]["puerto"]+' '+user_ssh+'@'+arrayAuxDbs[idxDb]["instanciaSalto"])
                    try:
                        util.ejecutor(cmds,False,"DB")
                    except:
                        try:
                            if perfil["nombre"] != 'ICBC':
                                print('Creando usuario SSH...')
                                listDirScripts = ['./aper_stack/aws_scripts/CreateLinuxDevUser.json','./aper_stack/aws_scripts/CreateLinuxAdminUser.json']
                                listaDir = [listDirScripts[i] for i in range(len(listDirScripts))]
                                i=1
                                for file_name in listaDir:
                                    print(str(i)+' : '+file_name)
                                    i=i+1
                                script_choice = input('ingrese: ')
                                resp = aws.execute_commands_on_linux_instances(perfil["perfil"],listaDir[int(script_choice)-1],arrayAuxDbs[idxDb]["instanciaSalto"],{'name':['db-user']})
                                print(resp)
                                print('Espere...')
                                time.sleep(30)
                                print('Copiando Claves ssh...')
                                aws.execute_commands_on_linux_instances(perfil["perfil"],'./aper_stack/aws_scripts/ImportSSHPublicKeyToDBUser.json',arrayAuxDbs[idxDb]["instanciaSalto"])
                            util.ejecutor(cmds,False,"DB")
                        except  Exception as e:
                            print(e)
                            util.printException('- Verifique que la instancia '+arrayAuxDbs[idxDb]["instanciaSalto"]+' Este encendida \n - Verifique que exista el usuario ubuntu en la misma.')
                        
                    break

if __name__ == '__main__':
    
    menuDbs()

