import os
import re
import glob
import pytz
import json
import time
import boto3
import util
import shutil
import daemon
import libtmux
import logging
import requests
import argparse
import readline
import datetime
from tqdm import tqdm
from pathlib import Path
from datetime import timedelta
from signal import signal, SIGINT
from boto3.session import Session
from botocore.exceptions import ClientError
from dateutil.parser import parse as DTparser
import globals

logging.basicConfig(filename='temp/aws.log',  level=logging.INFO)

instancias = json.load(open('./aper_stack/conf/aws/instancias.json'))

class instanciaObj: 
    def __init__(self, nombre, id,estado): 
        self.nombre = nombre 
        self.id = id
        self.estado = estado

def setOsSshConf(perfil,instanciasEc2Aux=False):
    if globals.config['certificado_ssh'] != '':
        if (instanciasEc2Aux == False):
            session = boto3.Session(
                profile_name = perfil["perfil"],
                region_name = 'us-east-1'
            )
            try:
                instanciasEc2Aux = session.resource('ec2').instances.all()
                instanciasEc2 = [i.id for i in instanciasEc2Aux]
            except:
                instanciasEc2 = [i["id"] for i in instancias]
        else:
            instanciasEc2 = [i.id for i in instanciasEc2Aux]
        instancias_aux =[str(i) for i in instanciasEc2]
        instancias_str = " ".join(instancias_aux)
        
        ssh_key = str(os.path.expanduser(globals.config['certificado_ssh']))
        
        with open(str(Path.home())+'/.ssh/config.d/'+perfil["nombre"], 'w', encoding='utf-8') as f:
            f.write(f'\n\thost {instancias_str}\n\tProxyCommand sh -c "aws ssm start-session --profile '+perfil["perfil"]+' --target %h --document-name AWS-StartSSHSession --parameters \'portNumber=%p\'" \n\tIdentityFile '+ ssh_key)
    else:
        util.printWarning('No tiene configurado certificado, no se configuraran los tuneles ssh.')

def setOsHostCredentials(creds, profile):
    globals.aws_credentials[profile] = {}
    globals.aws_credentials[profile]['aws_access_key_id'] = creds['AccessKeyId']
    globals.aws_credentials[profile]['aws_secret_access_key'] = creds['SecretAccessKey']
    globals.aws_credentials[profile]['aws_session_token'] = creds['SessionToken']
    globals.aws_credentials[profile]['region']='us-east-1'
    globals.aws_credentials.write()

def listarInstancias(perfil):
    session = boto3.Session(
        profile_name = perfil["perfil"],
        region_name = 'us-east-1'
    )
    instanciasAux = []
    arrayOpciones=[]
    try:
        instanciasEc2 = session.resource('ec2').instances.all()
        
        for instancia in instanciasEc2:
            auxNombre = None
            if(instancia.tags is not None):
                for t in instancia.tags:
                    if t['Key'] == 'Name':
                        auxNombre = t['Value'] + "("+instancia.id+")"
                    
            if (auxNombre is None):
                auxNombre = instancia.id
            
            if instancia.state["Name"] == 'stopped':
                colorOpt = util.bcolors.WARNING +auxNombre+' - '+instancia.state["Name"]+ util.bcolors.ENDC
            else:
                colorOpt = auxNombre+' - '+instancia.state["Name"]

            instanciasAux.append(instanciaObj(colorOpt,instancia.id,instancia.state["Name"]))
        
        instanciasAux = sorted(instanciasAux,key=lambda i: i.nombre)
        setOsSshConf(perfil,instanciasAux)
        arrayOpciones = [i.nombre for i in instanciasAux]
        arrayOpcionesExtra = {}
        arrayOpcionesExtra["?"] = "Filtro Busqueda"
        choice = util.menuMaker(arrayOpciones,'Seleccione Instancia',arrayOpcionesExtra)
        
        if choice == '0':
            return False
        elif choice == '?':
            filtro = input('Filtro: ')
            arrayFiltro = [k for k in arrayOpciones if filtro in k]
            instanciasAuxFiltro = [ka for ka in instanciasAux if filtro in ka.nombre]
            
            choice = util.menuMaker(arrayFiltro,'Filtro Instancia')
            if choice == '0':
                return False
            else:
                return instanciasAuxFiltro[int(choice)-1]        
        return instanciasAux[int(choice)-1]
    except Exception as e: 
        if 'expired' in str(e):
            print('borrando cache')
            util.ejecutor(["rm -r ${HOME}/.aws/sso"],False,"sso_login")
            print('conectando....')
            conectarPerfil(perfil,False)
            return False
        for i in instancias:
            instanciasAux.append(instanciaObj(i["nombre"],i["id"],''))
            arrayOpciones.append(i["nombre"]+' ('+i["id"]+')')
        
        choice = util.menuMaker(arrayOpciones,'Seleccione Instancia')
        setOsSshConf(perfil,instanciasAux)
        if choice == '0':
            return False
        
        return instanciasAux[int(choice)-1]

def execute_commands_on_linux_instances(perfil, commands, instance_ids,params=False):
    session = boto3.Session(
            profile_name = perfil,
            region_name = 'us-east-1'
    )
    client = session.client('ssm')
    
    with open(commands) as file :
        data = json.load(file)
        parameters = params
        if params == False:
            parameters = dict()
            for i in data['parameters'].keys():
                if (data['parameters'][i]=='sshkey'):
                    clave_ssh = open(os.path.expanduser(globals.config['certificado_ssh']+'.pub')).read().rstrip('\n')
                    val = [clave_ssh]
                elif (isinstance(data['parameters'][i],list)):
                    choice = util.menuMaker(data['parameters'][i],'Seleccione',clear=False)
                    if (choice == '0'):
                        return
                    val = [data['parameters'][i][int(choice)-1]]
                else:
                    return
                    
                parameters[i]=val
        
        print('aws ssm send-command --profile '+perfil+' --instance-ids '+instance_ids+'  --document-name '+data["documentName"]+' --output text --parameters '+json.dumps(parameters))

        client.send_command(
            DocumentName=data["documentName"], #"AWS-RunShellScript", # One of AWS' preconfigured documents
            Parameters=parameters,
            InstanceIds=[instance_ids],
        )
        return 'OK'

def isMFATokenExpired(fecha):
    MFAExpirationDate = DTparser(fecha)
    today = pytz.UTC.localize(datetime.datetime.now())
    if MFAExpirationDate < today + timedelta(minutes=200):
        return True
    else:
        return False

def authSSO(perfil):
    dir = os.path.expanduser('~/.aws/sso/cache')
    start_url = globals.aws_config['profile '+perfil]['sso_start_url']
    loguear = True
    if (os.path.exists(dir)):
        json_files = [pos_json for pos_json in os.listdir(dir) if pos_json.endswith('.json')]
        for json_file in json_files :
            path = dir + '/' + json_file
            with open(path) as file :
                data = json.load(file)
                if 'accessToken' in data and data['startUrl'] == start_url:
                    loguear = False
                    accessToken = data['accessToken']
                    if isMFATokenExpired(data["expiresAt"]) == True:
                        util.ejecutor(["aws sso login --profile "+perfil],False,"sso_login")
                    return accessToken

    if loguear == True:
        util.ejecutor(["aws sso login --profile "+perfil],False,"sso_login")
    return True

def authMFA(perfil):
    cred =  [obj for obj in globals.config["sts"] if obj['nombre']==perfil["owner"]]
    if len(cred):
        cred = cred[0]
        nuevo = True
        if (os.path.isfile('./temp/temp_'+str(cred["nombre"])+'_aws.json') == True):
            MFA_validated_token = json.load(open('./temp/temp_'+str(cred["nombre"])+'_aws.json'))
            if (isMFATokenExpired(MFA_validated_token['Credentials']['Expiration']) == False):
                nuevo = False

        if (nuevo == True):
            session = boto3.Session(
                aws_access_key_id=cred["aws_access_key_id"],
                aws_secret_access_key=cred["aws_secret_access_key"],
                region_name=cred["region"]
            )

            mfa_serial = cred['mfa_serial']
            mfa_token = input('El token che:')

            sts = session.client('sts')
            MFA_validated_token = sts.get_session_token(SerialNumber=mfa_serial, TokenCode=mfa_token)

            with open('./temp/temp_'+str(cred["nombre"])+'_aws.json', 'w', encoding='utf-8') as f:
                aux = MFA_validated_token
                aux['Credentials']['Expiration'] = str(aux['Credentials']['Expiration'])
                json.dump(MFA_validated_token, f, ensure_ascii=False, indent=4)

        return MFA_validated_token
    else:
        return util.printException('No Existen Credenciales')

def assumeRole(perfil):
    try:
        perfiles = [{"perfil":obj.lstrip('profile').replace(" ", ""),"owner":globals.aws_config[obj]["owner"],"rol":globals.aws_config[obj]["rol"],"account_id":globals.aws_config[obj]["account_id"],"auth_aws":globals.aws_config[obj]["auth_aws"] } for obj in globals.aws_config if globals.aws_config[obj]["auth_aws"] == 'sts' and perfil["nombre"] == globals.aws_config[obj]["nombre"] and perfil["rol"] == globals.aws_config[obj]["rol"] ]
        perfil = perfiles[0]
        
        auth_token = authMFA(perfil)
        if auth_token != False:
            session = Session(aws_access_key_id=auth_token['Credentials']['AccessKeyId'],
                        aws_secret_access_key=auth_token['Credentials']['SecretAccessKey'],
                        aws_session_token=auth_token['Credentials']['SessionToken']
                    )

            assumed_role_object=session.client('sts').assume_role(
                RoleArn='arn:aws:iam::'+perfil["account_id"]+':role/'+perfil["rol"], #"arn:aws:iam::196375641185:role/developers-ssm",
                RoleSessionName=globals.config["usuario"]
            )
            rta=assumed_role_object['Credentials']
            return rta
        else:
            return False
    except ClientError as e:
        return util.printException(e.response['Error']['Code'])

def opcionesConexion(instancia,modo,perfil,perfil_nombre):
    arrayOpciones = []
    arrayOpciones.append("Terminal (SSM)")
    if globals.config['certificado_ssh'] != '':
        arrayOpciones.append("Terminal (SSH)")
        arrayOpciones.append("Ruteo Manual (SSH)")
        arrayOpciones.append("Dynamic Mount Point (DMP82)")
        arrayOpciones.append("Remote Scripts")
    opcion = util.menuMaker(arrayOpciones,'Conexion')
    if opcion == '0' or not 1 <= int(opcion) < 6:
        return False
    else:
        cmds  = list()
        if opcion == '1':
            if modo == 'aws-docker':
                cmds.append('docker run -it -e AWS_DEFAULT_REGION=us-east-1  aper-aws-cli  ssm start-session --profile '+perfil+' --target '+instancia)
                return cmds
            if modo == 'aws-host':
                cmds.append('aws ssm start-session --profile '+perfil+' --target '+instancia)
                return cmds
        elif opcion == '2':
            usr_choice = util.menuMaker(globals.usuarios_ssh,'Seleccione Usuario',[],False)
            if usr_choice == '0':
                return False
            else:
                user_ssh = globals.usuarios_ssh[int(usr_choice)-1]
            if perfil_nombre != 'ICBC':
                execute_commands_on_linux_instances(perfil,'./aper_stack/aws_scripts/ImportSSHPublicKeyToDBUser.json',instancia)
            cmds.append('ssh '+user_ssh+'@'+instancia)
            return  cmds
        elif opcion == '3':
            print('Ingrese ruteo avanzado en el formato de ssh ej: -L 1250:bice-db-dev.cluster-ci0pfqreqrkr.us-east-1.rds.amazonaws.com:3306 -L 1251:localhost:1885')
            regla = input()
            usr_choice = util.menuMaker(globals.usuarios_ssh,'Seleccione Usuario',[],False)
            if usr_choice == '0':
                return False
            else:
                user_ssh = globals.usuarios_ssh[int(usr_choice)-1]
            
            if perfil_nombre != 'ICBC':
                execute_commands_on_linux_instances(perfil,'./aper_stack/aws_scripts/ImportSSHPublicKeyToDBUser.json',instancia)
            cmds.append('ssh'+' '+regla+' '+user_ssh+'@'+instancia)
            return cmds
        elif opcion == '4':
            usr_choice = util.menuMaker(globals.usuarios_ssh,'Seleccione Usuario',[],False)
            if usr_choice == '0':
                return False
            else:
                user_ssh = globals.usuarios_ssh[int(usr_choice)-1]
            
            if perfil_nombre != 'ICBC':
                execute_commands_on_linux_instances(perfil,'./aper_stack/aws_scripts/ImportSSHPublicKeyToDBUser.json',instancia)
            print('Conectando a ./mnt')
            cmds.append('sshfs -f -o allow_other '+user_ssh+'@'+instancia+':/var/www mnt/')
            return cmds
        elif opcion == '5':
            listDirScripts = glob.glob('./aper_stack/aws_scripts/*.json')
            listaDir = [listDirScripts[i] for i in range(len(listDirScripts))]
            i=1
            for file_name in listaDir:
                print(str(i)+' : '+file_name)
                i=i+1
            script_choice = input('ingrese: ')
            resp = execute_commands_on_linux_instances(perfil,listaDir[int(script_choice)-1],instancia)
            input(resp)
            
            return []
        else:
            return False
               
def anadirPerfil():
    try:
        #Ask aper o icbc
        perfilesAper = [obj for obj in globals.cuentas if obj["owner"]=='aper']
        perfilesAperStr = [obj["nombre"] for obj in perfilesAper]
        c_perfil = util.menuMaker(perfilesAperStr,'Perfil')

        modo_auth = ["APER-STS","APER-SSO","ICBC"]
        c_auth = util.menuMaker(modo_auth,'Autenticacion')

        arrayRoles = [obj["roles"] for obj in globals.roles if obj["owner"]==modo_auth[int(c_auth)-1].lower()][0]
        c_rol  = util.menuMaker(arrayRoles,'Rol')

        perfil = perfilesAper[int(c_perfil)-1]
        rol = arrayRoles[int(c_rol)-1]
        
        if (c_auth == '1'):
            globals.aws_config["profile "+perfil["nombre"]+"-"+rol]={}
            globals.aws_config["profile "+perfil["nombre"]+"-"+rol]['region']='us-east-1'
            globals.aws_config["profile "+perfil["nombre"]+"-"+rol]["nombre"]=perfil["nombre"]
            globals.aws_config["profile "+perfil["nombre"]+"-"+rol]["owner"]=perfil["owner"]
            globals.aws_config["profile "+perfil["nombre"]+"-"+rol]["rol"] = rol
            globals.aws_config["profile "+perfil["nombre"]+"-"+rol]["auth_aws"] = 'sts'
            globals.aws_config["profile "+perfil["nombre"]+"-"+rol]["account_id"] = perfil["account_id"]
            globals.aws_config["profile "+perfil["nombre"]+"-"+rol]["auth_aws"]='sts'
        else:
            globals.aws_config["profile "+perfil["nombre"]+"-"+rol]={}
            globals.aws_config["profile "+perfil["nombre"]+"-"+rol]['nombre']=perfil["nombre"]
            globals.aws_config["profile "+perfil["nombre"]+"-"+rol]['region']='us-east-1'
            globals.aws_config["profile "+perfil["nombre"]+"-"+rol]["owner"]=perfil["owner"]
            globals.aws_config["profile "+perfil["nombre"]+"-"+rol]["sso_region"]='us-east-1'
            globals.aws_config["profile "+perfil["nombre"]+"-"+rol]["sso_start_url"]='https://aper.awsapps.com/start'
            globals.aws_config["profile "+perfil["nombre"]+"-"+rol]["sso_account_id"]=perfil["account_id"]
            globals.aws_config["profile "+perfil["nombre"]+"-"+rol]["sso_role_name"]=rol
            globals.aws_config["profile "+perfil["nombre"]+"-"+rol]["auth_aws"]='sso'

        globals.aws_config.write()

        return
    except ClientError as e:
        return util.printException(e.response['Error']['Code'])

def eliminarPerfil():
    nro_perfil = input("\n Ingrese perfil a eliminar (0 Volver): ")
    tes = [obj for obj in globals.aws_config]
    
    if nro_perfil == '0':
        return
    
    index = int(nro_perfil)-1
    globals.aws_config[tes[index]]=''
    globals.aws_config.write()
    globals.load()

def conectarPerfil(perfil,tmux):
    logging.info('##############conectarPefil')
    try:
        if (perfil["auth_aws"].lower() == 'sso'):
            authSSO(perfil["perfil"])
        else:
            token_temp = assumeRole(perfil)
            setOsHostCredentials(token_temp,perfil["perfil"])

        instancia = listarInstancias(perfil)
        if instancia == False:
            return
        cmds = opcionesConexion(instancia.id,globals.config["modo"],perfil["perfil"],perfil["nombre"])
        logging.info(cmds)
        if  cmds == False:
            return
        util.ejecutor(cmds,tmux,perfil["perfil"])
        
    except ClientError as e:
        return util.printException(e.response['Error']['Code'])

def downloadS3File(bucket,origen,destino,perfil):
    logging.info('###############downloadS3File:')
    
    aux = sorted([{"perfil":obj.lstrip('profile').replace(" ", ""),"nombre":globals.aws_config[obj]["nombre"],"rol": globals.aws_config[obj]["rol"] if "rol" in dict(globals.aws_config[obj]).keys() is not None else globals.aws_config[obj]["sso_role_name"],"auth_aws":globals.aws_config[obj]["auth_aws"] } for obj in globals.aws_config],key=lambda i: i["perfil"])
    perfiles = []
    for a in aux:
        if (a['nombre'].lower() == perfil.lower()):
            perfiles.append(a)
    if len(perfiles) == 0:
        raise Exception('No Existen Perfiles con este nombre')
    elif len(perfiles) == 1:
        perfil = perfiles[0]
    else:
        i = 1
        for p in perfiles:
            print (str(i)+" : "+p["perfil"])
            i = i+1
        
        inpPerfil = input("Seleccione Perfil que desea usar: ")
        idxPerfil = int(inpPerfil)-1
        perfil = perfiles[idxPerfil]

    if (perfil["auth_aws"].lower() == 'sso'):
        authSSO(perfil["perfil"])
    else:
        token_temp = assumeRole(perfil)
        setOsHostCredentials(token_temp,perfil["perfil"])
    
    session = Session(
        profile_name = perfil["perfil"]
    )
    s3 = session.client('s3')
    s3.download_file(bucket,origen, destino)

def checkAwsFile():
    for c in globals.aws_config:
        aux = globals.aws_config[c].keys()
        if 'nombre' not in aux:
            del(globals.aws_config[c])
            continue
        if 'auth_aws' not in aux:
            del(globals.aws_config[c])
            continue
        if 'account_id' not in aux and 'sso_account_id' not in aux:
            del(globals.aws_config[c])
            continue

    globals.aws_config.write()
    globals.load()
    return True
    
def menuAws(tmux=False):
    while True:
        try:
            arrayOpciones = []
            arrayOpciones = sorted([{"perfil":obj.lstrip('profile').replace(" ", ""),"nombre":globals.aws_config[obj]["nombre"],"rol": globals.aws_config[obj]["rol"] if "rol" in dict(globals.aws_config[obj]).keys() is not None else globals.aws_config[obj]["sso_role_name"],"auth_aws":globals.aws_config[obj]["auth_aws"] } for obj in globals.aws_config],key=lambda i: i["perfil"])
            arrayOpcionesStr = [obj["perfil"] for obj in arrayOpciones]
            arrayOpcionesExtra = {}
            arrayOpcionesExtra["AP"] = "Añadir Perfil"
            arrayOpcionesExtra["EP"] = "Eliminar Perfil"

            choice  = util.menuMaker(arrayOpcionesStr,'AWS',arrayOpcionesExtra)
            choice = choice.lower()

            if choice == 'ap':
                anadirPerfil()
                return
            if choice == 'ep':
                eliminarPerfil()
                return
            elif choice == '0' or choice == '' or not 1 <= int(choice) <= len(arrayOpcionesStr):
                return
            elif 1 <= int(choice) <= len(arrayOpcionesStr):
                conectarPerfil(arrayOpciones[int(choice)-1],tmux)
            else:
                return

        except Exception as e:
            logging.info(str(e))
            util.printException(e)
            return False

def inicioAws():
    menuAws()

def renew(profile_cmd,output=None):
    aux = sorted([{"perfil":obj.lstrip('profile').replace(" ", ""),"account_id":globals.aws_config[obj]["account_id"] if "account_id" in dict(globals.aws_config[obj]).keys() is not None else globals.aws_config[obj]["sso_account_id"],"region":globals.aws_config[obj]["region"],"nombre":globals.aws_config[obj]["nombre"],"rol": globals.aws_config[obj]["rol"] if "rol" in dict(globals.aws_config[obj]).keys() is not None else globals.aws_config[obj]["sso_role_name"],"auth_aws":globals.aws_config[obj]["auth_aws"] } for obj in globals.aws_config],key=lambda i: i["perfil"])
    perfiles = []
    for a in aux:
        if (profile_cmd.lower() in a['perfil'].lower()):
            perfiles.append(a)
    if len(perfiles) == 0:
        print('No Existen Perfiles con este nombre')
        return 
        input()
    elif len(perfiles) == 1:
        perfil = perfiles[0]
    else:
        i = 1
        for p in perfiles:
            print (str(i)+" : "+p["perfil"])
            i = i+1
        
        inpPerfil = input("Seleccione Perfil que desea usar: ")
        idxPerfil = int(inpPerfil)-1
        perfil = perfiles[idxPerfil]

    
    if (perfil["auth_aws"].lower() == 'sso'):
        accessToken = authSSO(perfil["perfil"])
        client = boto3.client('sso', region_name=perfil['region'])
        
        response = client.get_role_credentials(
            roleName=perfil["rol"],
            accountId=perfil["account_id"],
            accessToken=accessToken,
        )
        if output == None:
            print (response["roleCredentials"])
        else:
            auxRutaPysed =  shutil.which("pysed")
            cmds = []
            cmds.append(auxRutaPysed+" -r \"(AWS_ACCESS_KEY_ID.*)\" \"AWS_ACCESS_KEY_ID="+response["roleCredentials"]["accessKeyId"]+"\" "+output+"  --write")
            cmds.append(auxRutaPysed+" -r \"(AWS_SECRET_ACCESS_KEY.*)\" \"AWS_SECRET_ACCESS_KEY="+response["roleCredentials"]["secretAccessKey"]+"\" "+output+"  --write")
            cmds.append(auxRutaPysed+" -r \"(AWS_SESSION_TOKEN.*)\" \"AWS_SESSION_TOKEN="+response["roleCredentials"]["sessionToken"]+"\" "+output+"  --write")
            util.ejecutor(cmds,"",False)
            
    else:
        token_temp = assumeRole(perfil)
        setOsHostCredentials(token_temp,perfil["perfil"])

    setOsSshConf(perfil)
    print(perfil["perfil"]+' renovado con exito')

if __name__ == '__main__':
    #args = parser.parse_args()
    #print(globals.aws_config['profile SantanderMX-Developer']['sso_start_url'])
    inicioAws()
