#!/bin/bash
sudo apt-get update
sudo apt install python3 python3-pip tmux sshfs unzip

echo 'set -g mouse on' >  $HOME/.tmux.conf
echo 'set -g set-clipboard off' >>  $HOME/.tmux.conf 

if ! [ -d "temp" ]; then
    mkdir temp
fi

if ! [ -d "temp/db" ]; then
    mkdir temp/db/
    mkdir temp/db/dumps
fi

if ! [ -d "mnt" ]; then
    mkdir mnt
fi

if ! [ -d "stores" ]; then
    mkdir stores
fi
if ! [ -d "lambdas" ]; then
    mkdir lambdas
fi
if ! [ -d "ms" ]; then
    mkdir ms
fi
if ! [ -d "aper_stack/docker/stores/nginx/sites-enable" ]; then
    mkdir aper_stack/docker/stores/nginx/sites-enable
fi

if ! [ -d "$HOME/.ssh/" ]; then
    mkdir $HOME/.ssh/
fi
if ! [ -d "$HOME/.ssh/config.d" ]; then
    mkdir $HOME/.ssh/config.d
fi

if ! [ -d "$HOME/.aws/" ]; then
    mkdir $HOME/.aws/
    touch $HOME/.aws/config
fi

rm $HOME/.ssh/config.d/*

if ! grep -Fxq "Match all" $HOME/.ssh/config; then
    echo 'Match all' >> $HOME/.ssh/config
fi
if ! grep -Fxq "Include config.d/*" $HOME/.ssh/config; then
    echo 'Include config.d/*' >> $HOME/.ssh/config
fi

pip3 install -r requirements.txt

if ! test -f ".env"; then
    cp .env.example .env
fi

touch $HOME/.bash_aliases
AA="cd ${PWD} && python3 aper.py"
AVUE="cd ${PWD} && docker-compose run node sh"
if ! grep -Fxq "alias aper='$AA'" $HOME/.bash_aliases; then
    echo "alias aper='$AA'" >> $HOME/.bash_aliases
fi

if ! grep -Fxq "alias aper-vue='$AVUE'" $HOME/.bash_aliases; then
    echo "alias aper-vue='$AVUE'" >> $HOME/.bash_aliases
fi

source $HOME/.bash_aliases
