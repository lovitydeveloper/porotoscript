import os
import re
import glob
import json
import subprocess
from tqdm import tqdm
import git
import fileinput
import libtmux
import util
from pathlib import Path
from aws import downloadS3File
from PIL import Image
import shutil
import globals
from configobj import ConfigObj


def clone(entorno):    
    passwordHelperCmd = 'git config --global credential.helper cache'
    process = subprocess.Popen(passwordHelperCmd.split(), stdout=subprocess.PIPE).wait()
    repo_list = []
    raiz = './stores/'+entorno['nombre']+'/'
    eliminar = True

    if(os.path.isdir(raiz)):
        print('El directorio existe, Eliminar? S/N')
        opcion = input()
        if opcion.lower() =='s':
            cmd = "sudo rm -r "+raiz
            process = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE).wait()
        else:
            eliminar = False

    if eliminar:
        for item in tqdm(entorno['repos']):
            git.Repo.clone_from(item['url'], raiz+item['dir'], 
            branch=item['branch'])

def copyVendor(entorno,store_dir):
    if (entorno['version']=='1.7'):
        if (os.path.isdir(store_dir+"vendor") == False):
            os.mkdir(store_dir+"vendor")
            util.ejecutor(['tar -zxvf '+"./aper_stack/extras/vendor.tgz"+' -C '+store_dir+"vendor"],False,'Vendor')

def copyAmbiente(entorno,ambiente):
    dest_dir = './stores/'+entorno["nombre"]+'-'+ambiente["nombre"]+'/'
    if (os.path.isdir(dest_dir) == False):
        os.mkdir(dest_dir)
    else:
        inp = input('El store ya existe desea sobrescribir? S/N : ')
        if (inp.lower() == 'n'):
            return
    
    downloadS3File(ambiente["s3CodeBucket"],ambiente["s3CodeDump"],dest_dir+"code.tgz",entorno["perfil"])
    util.ejecutor(['tar -zxvf '+dest_dir+'code.tgz -C '+dest_dir],False,'sss')

def init(nombre_tienda,entorno,ambiente,store_dir):
    cmds = list()
    auxRutaPysed =  shutil.which("pysed") #str(Path.home())+'/.local/bin/pysed'
    dbhost = 'aper_mysql-db'
    if ambiente["databaseVersion"] == "8":
        dbhost ='aper_mysql8-db'
    if entorno["version"] == '1.7':
        php_ver = 'php7.3'
        cmds.append(auxRutaPysed+" -r \"('database_host'.*)\" \"'database_host'=>'"+dbhost+"',\" "+store_dir+"app/config/parameters.php  --write")
        cmds.append(auxRutaPysed+" -r \"('database_port'.*)\" \"'database_port'=>'3306',\" "+store_dir+"app/config/parameters.php  --write")
        cmds.append(auxRutaPysed+" -r \"('database_user'.*)\" \"'database_user'=>'root',\" "+store_dir+"app/config/parameters.php  --write")
        cmds.append(auxRutaPysed+" -r \"('database_password'.*)\" \"'database_password'=>'32fh78egbfvh932hf897wef3',\" "+store_dir+"app/config/parameters.php  --write")

    else:
        php_ver = 'php7.1'
        cmds.append(auxRutaPysed+" -r \"('_DB_SERVER_'.*)\" \"'_DB_SERVER_', '"+dbhost+"');\" "+store_dir+"config/settings.inc.php  --write")
        cmds.append(auxRutaPysed+" -r \"('_DB_USER_'.*)\" \"'_DB_USER_', 'root');\" "+store_dir+"config/settings.inc.php  --write")
        cmds.append(auxRutaPysed+" -r \"('_DB_PASSWD_'.*)\" \"'_DB_PASSWD_', '32fh78egbfvh932hf897wef3');\" "+store_dir+"config/settings.inc.php  --write")
    
    cmds.append('cp ./aper_stack/docker/stores/nginx/default.conf.template ./aper_stack/docker/stores/nginx/sites-enable/'+nombre_tienda+'.conf')
    cmds.append(auxRutaPysed+" -r \">>>STORE\" "+nombre_tienda+"  ./aper_stack/docker/stores/nginx/sites-enable/"+nombre_tienda+".conf --write")
    cmds.append(auxRutaPysed+" -r \">>>PHP_VER\" "+php_ver+"  ./aper_stack/docker/stores/nginx/sites-enable/"+nombre_tienda+".conf --write")
    cmds.append(auxRutaPysed+" -r \">>>PRESTA_VER\" "+entorno["version"]+"  ./aper_stack/docker/stores/nginx/sites-enable/"+nombre_tienda+".conf --write")
    cmds.append("docker  exec -t aper_nginx nginx -s reload")
    cmds.append("echo 127.0.0.1 "+nombre_tienda+".local |sudo tee -a /etc/hosts")
    cmds.append("sudo chmod -R 777  "+store_dir)
    if (globals.config["modo_gitlab"] == 'HTTPS'):
        cmds.append("cd "+store_dir+" && for dir in $(find .  -name \".git\" -not -path \"*/vendor/*\" -not -path \"./var\" |xargs dirname);do ( cd $dir && echo \"$dir [$( "+auxRutaPysed+" -r git@gitlab.com:  https://gitlab.com/ .git/config --write)]\") ; done")
    cmds.append("cd "+store_dir+" && for dir in $(find .  -name \".git\" -not -path \"*/vendor/*\" -not -path \"./var\" |xargs dirname);do ( cd $dir && echo \"$dir [$( git config core.fileMode false)]\") ; done")

    util.ejecutor(cmds,False,"Init")

def dumpDb(entorno,ambiente):
    dest_dir = './temp/db/dumps/'
    archivo = dest_dir+entorno["nombre"]+'-'+ambiente["nombre"]+"-dump.sql.gz"
    downloadS3File(ambiente["s3DbBucket"],ambiente["s3DbDump"],archivo,entorno["perfil"])
    
    cmds = list()
    if os.path.isfile(archivo):
        cmds.append('gunzip -f '+archivo)
    util.ejecutor(cmds,False,'Cp Dump Db '+entorno['nombre'])

def restaurarDb(entorno,ambiente):
    cmds=[]
    archivo = entorno["nombre"]+'-'+ambiente["nombre"]+"-dump.sql"
    dbhost = 'aper_mysql-db'
    if ambiente["databaseVersion"] == "8":
        dbhost ='aper_mysql8-db'
    cmds.append('docker exec -i '+dbhost+'  sh -c \'mysql -u root --password=32fh78egbfvh932hf897wef3 -e "DROP DATABASE IF EXISTS '+entorno['nombre']+'; CREATE DATABASE '+entorno['nombre']+';"\'')
    cmds.append('docker exec -i '+dbhost+'  sh -c \'mysql -u root --password=32fh78egbfvh932hf897wef3 '+entorno['nombre']+' < /dumps/'+archivo+'\'')
    util.ejecutor(cmds,False,'Cp Dump Db '+entorno['nombre'])

def updateSentencesStore(nombre_tienda,ambiente):
    cmds=[]
    with open('./temp/db/dumps/updateStore_'+nombre_tienda, 'w', encoding='utf-8') as f:
        f.write("use "+ambiente["databaseName"]+";")
        f.write("update ps_shop_url set domain = '"+nombre_tienda+".local:10443', domain_ssl = '"+nombre_tienda+".local:10443';")
        f.write("update ps_configuration set value = '"+nombre_tienda+".local:10443' where name like '%SHOP_DOMAIN%';")
        f.write("update ps_configuration set value = '1' where name like '%SSL_ENABLED%';")
        f.write("update ps_configuration set name = 'RECAPTCHA_API_KEY_off' where name = 'RECAPTCHA_API_KEY';")
        f.write("update ps_configuration set name = 'RECAPTCHA_KEY_off' where name = 'RECAPTCHA_KEY';")
        f.write("update ps_configuration set name = 'RECAPTCHA_SECRET_KEY_off' where name = 'RECAPTCHA_SECRET_KEY';")
        f.write("update ps_configuration set value = null where name like '%PS_MEDIA_SERVER%';")
    
    dbhost = 'aper_mysql-db'
    if ambiente["databaseVersion"] == "8":
        dbhost ='aper_mysql8-db'
    cmds.append('docker exec -i '+dbhost+'  sh -c \'mysql -u root --password=32fh78egbfvh932hf897wef3  < /dumps/updateStore_'+nombre_tienda+' \'')
    util.ejecutor(cmds,False,'Update store Db '+nombre_tienda)

def usuarioBackoffice(entorno,ambiente,store_dir):
    nombre = input('Nombre: ')
    apellido = input('Apellido: ')
    email = input('Email: ')
    passwd = input('Password: ')

    if (entorno['version']=='1.7'):
        with open(store_dir+"app/config/parameters.php") as origin_file:
            for line in origin_file:
                a = re.findall(r'\bcookie_key\b', line)
                if a:
                    cookie = line.split("'")[3]
                    tblSecureTry = True
    else:
        with open(store_dir+"config/settings.inc.php") as origin_file:
            for line in origin_file:
                a = re.findall(r'_COOKIE_KEY_', line)
                if a:
                    cookie = line.split("'")[3]
                    tblSecureTry = False

    passcookie = cookie+passwd

    cmds=[]
    id_employee = 3
    with open('./temp/db/dumps/userBo_'+entorno['nombre'], 'w', encoding='utf-8') as f:
        f.write("use "+ambiente['databaseName']+";")
        f.write("update ps_employee set email = 'desconectado' where email ='"+email+"';")
        sql = (""" 
            UPDATE ps_employee SET 
            `lastname` = '{0}', 
            `firstname` = '{1}', 
            `email` = '{2}', 
            `passwd` = MD5('{3}'), 
            `id_profile` = 1,
            `last_passwd_gen` = NOW(), 
            `stats_date_from` = CURRENT_DATE(), 
            `stats_date_to` = CURRENT_DATE(), 
            `last_connection_date` = CURRENT_DATE(), 
            `active` = 1 
            WHERE (`id_employee` = '{4}'); """).format(apellido,nombre,email,passcookie,id_employee)
        f.write(sql)

        if tblSecureTry == True:
            sql2 = (""" 
                UPDATE ps_employee_secure_login_trys SET 
                `trys` = 0
                WHERE (`id_employee` = '{0}');""").format(id_employee)
            f.write(sql2)

    dbhost = 'aper_mysql-db'
    if ambiente["databaseVersion"] == "8":
        dbhost ='aper_mysql8-db'
    cmds.append('docker exec -i '+dbhost+'  sh -c \'mysql -u root --password=32fh78egbfvh932hf897wef3  < /dumps/userBo_'+entorno['nombre']+'\'')
    util.ejecutor(cmds,False,'Update user BO ')
    print('Usuario Creado.')

def generarImagenesStore(store_dir,entorno):
    cmds = []
    dbhost = 'aper_mysql-db'
    if entorno["databaseVersion"] == "8":
        dbhost ='aper_mysql8-db'
    cmds.append('docker exec -i '+dbhost+'  sh -c \'mysql -u root --password=32fh78egbfvh932hf897wef3 -e "use '+entorno['databaseName']+'; SELECT id_image FROM ps_image limit 1000;"\' > ./temp/id_images')
    util.ejecutor(cmds,False,'query imgs ')

    with open('./temp/id_images', 'r', encoding='utf-8') as infile:
        for x in infile.readlines()[1:-1]:
            auxchar = list(str(x).strip())
            auxruta = ''
            for c in auxchar:
                auxruta += c+'/'
                if (os.path.isdir(store_dir+'img/p/'+auxruta) == False):
                    os.mkdir(store_dir+'img/p/'+auxruta)
                
            img = Image.new('RGB', (100,100), color=(73, 109, 137))
            img.save(store_dir+'img/p/'+auxruta+str(x.strip())+'.jpg')
            img.save(store_dir+'img/p/'+auxruta+str(x.strip())+'-small_default.jpg')
            img.save(store_dir+'img/p/'+auxruta+str(x.strip())+'-large_default.jpg')
            img.save(store_dir+'img/p/'+auxruta+str(x.strip())+'-medium_default.jpg')
            img.save(store_dir+'img/p/'+auxruta+str(x.strip())+'-pdt_360.jpg')
            img.save(store_dir+'img/p/'+auxruta+str(x.strip())+'-pdt_540.jpg')

def generarDebugVscode(store_dir,entorno,ambiente):
    if (os.path.isdir(store_dir+".vscode") == False):
        os.mkdir(store_dir+".vscode")
    vscode_template = json.load(open('./aper_stack/conf/entornos/debug/vscode_tpl.json'))
    mapping = {}
    mapping['/var/www/'+entorno["nombre"]+'-'+ambiente["nombre"]] = '${workspaceFolder}'
    if entorno["version"] == '1.7':
        vscode_template["configurations"][0]["port"]=9002
    else:
        vscode_template["configurations"][0]["port"]=9001

    vscode_template["configurations"][0]["name"]='Xdebug '+entorno["nombre"]+'-'+ambiente["nombre"]
    vscode_template["configurations"][0]["pathMappings"]= mapping
    with open(store_dir+".vscode"+"/launch.json", 'w', encoding='utf-8') as archivo:
            json.dump(vscode_template, archivo, ensure_ascii=False, indent=4)       

def initAmbiente(entorno,idxAmbiente,modoInstalacion):
    ambiente = entorno["ambientes"][idxAmbiente]
    nombre_tienda = entorno["nombre"]+'-'+ambiente["nombre"]
    store_dir = './stores/'+nombre_tienda+"/"

    if modoInstalacion == 'Automatica':
        try:
            print('#################################Source Code#################################')
            copyAmbiente(entorno,ambiente)
            
            print('#################################DB###############################')
            dumpDb(entorno,ambiente)
            restaurarDb(entorno,ambiente)
            
            print('#################################VENDOR###############################')
            copyVendor(entorno,store_dir)
            print('#################################MAKE IMGS###############################')
            if (os.path.isdir(store_dir+"img/p") == False):
                os.mkdir(store_dir+"img/p")
            if ambiente["imgS3"] == False:
                generarImagenesStore(store_dir,ambiente)

            print('#################################Update MYSQL###############################')
            updateSentencesStore(nombre_tienda,ambiente)

            print('#################################Usuario Backoffice###############################')
            usuarioBackoffice(entorno,ambiente,store_dir)

            print('#################################Debug VSCODE###############################')
            generarDebugVscode(store_dir,entorno,ambiente)
            
            print('#################################INIT####################################')
            init(nombre_tienda,entorno,ambiente,store_dir)

            print('Sitio Completo: https://'+nombre_tienda+'.local:10443')
            input('Presione una tecla para continuar...')
        except Exception as e:
                
                util.printException(e)
    else:
        while True:
            arrayOpciones = []
            arrayOpciones.append("Copiar Ambiente")
            arrayOpciones.append("Copiar Dump DB")
            arrayOpciones.append("Restaurar DB")
            arrayOpciones.append("Copiar Vendor")
            arrayOpciones.append("Crear Imgs")
            arrayOpciones.append("Update querys")
            arrayOpciones.append("Usuario Backoffice")
            arrayOpciones.append("Generar perfil debug VScode")
            arrayOpciones.append("Inicializar")

            opcion  = util.menuMaker(arrayOpciones,'Instalacion Manual'+entorno['nombre'])
            try:
                if opcion == '1':
                    print ('Copiando Ambiente')
                    copyAmbiente(entorno,ambiente)
                    input('Completo. Presione una tecla para continuar')
                elif opcion == '2':
                    print ('Copiando DB')
                    dumpDb(entorno,ambiente)
                    input('Completo. Presione una tecla para continuar')
                elif opcion == '3':
                    restaurarDb(entorno,ambiente)
                    print('completo')
                elif opcion == '4':
                    print ('Copiando Vendor')
                    copyVendor(entorno,store_dir)
                    input('Completo. Presione una tecla para continuar')
                elif opcion == '5':
                    print ('Creando Imagenes')
                    if (os.path.isdir(store_dir+"img/p") == False):
                        os.mkdir(store_dir+"img/p")
                    if ambiente["imgS3"] == False:
                        generarImagenesStore(store_dir,ambiente)
                    input('Completo. Presione una tecla para continuar')
                elif opcion == '6':
                    print ('Querys')
                    updateSentencesStore(nombre_tienda,ambiente)
                    input('Completo. Presione una tecla para continuar')
                elif opcion == '7':
                    print ('Usuario backoffice')
                    usuarioBackoffice(entorno,ambiente,store_dir)
                    input('Completo. Presione una tecla para continuar')
                elif opcion == '8':
                    print ('Minando cripto...espere')
                    generarDebugVscode(store_dir,entorno,ambiente)
                    input('Completo. Presione una tecla para continuar')
                elif opcion == '9':
                    print ('Normalizando el condensador de flujo')
                    init(nombre_tienda,entorno,ambiente,store_dir)
                    print('Sitio Completo: https://'+nombre_tienda+'.local:10443')
                    input('Completo. Presione una tecla para continuar')
                else:
                    return
            
            except Exception as e:
                util.printException(e)

def entornos():
    entornosDir = './aper_stack/conf/entornos/stores'
    archivos = os.listdir(entornosDir)
    modoInstalacion = 'Automatica'
    while True:
        otrasOpciones = {}
        otrasOpciones['99'] = 'Instalacion '+util.bcolors.OKGREEN +modoInstalacion+util.bcolors.ENDC
        opcionEntorno = util.menuMaker(archivos,'Stores',otrasOpciones)
        if opcionEntorno == '0':
            return
        if opcionEntorno == '99':
            if modoInstalacion == 'Automatica':
                modoInstalacion = 'Manual'
            else:
                modoInstalacion = 'Automatica'
        
        else:
            entorno_archivo = open (os.path.join(entornosDir, archivos[int(opcionEntorno)-1]))
            entorno = json.load(entorno_archivo)
            arrayAmbientes = [a['nombre'] for a in entorno["ambientes"] ]
            
            opcionAmbiente = util.menuMaker(arrayAmbientes,'amb')
            if opcionAmbiente == '0':
                break
            initAmbiente(entorno,int(opcionAmbiente)-1,modoInstalacion)
    
def lambdaGen():
    lambdas = os.listdir('./lambdas')
    otrasOpciones = {}
    otrasOpciones['99'] = 'Crear Nueva'
    while True:
        opcionLambda = util.menuMaker(lambdas,'Lambdas',otrasOpciones)
        if opcionLambda == '0':
            return
        if opcionLambda == '99':
            nombre = input('Ingrese el nombre: ')
            envFile = ConfigObj('.env')
            red = envFile['RED']
            
            auxRutaPysed =  shutil.which("pysed")
            cmds =[]
            cmds.append('cp -r aper_stack/templates/lambda lambdas/'+nombre)
            cmds.append('cp  lambdas/'+nombre+'/.env.example lambdas/'+nombre+'/.env')
            
            util.ejecutor(cmds,False,'LAMBDA GEN')
            
            cmds =[]
            cmds.append(auxRutaPysed+" -r \">>>NOMBRE\" "+nombre+"  ./lambdas/"+nombre+"/.env --write")
            cmds.append(auxRutaPysed+" -r \">>>RED\" "+red+"  ./lambdas/"+nombre+"/.env --write")
            cmds.append(auxRutaPysed+" -r \">>>IP\" "+util.getIp()+"  ./lambdas/"+nombre+"/.env --write")
            
            util.ejecutor(cmds,False,'LAMBDA GEN')

            package_json = json.load(open('./lambdas/'+nombre+'/package.json'))
            package_json["name"] = nombre
            with open('./lambdas/'+nombre+'/package.json', 'w', encoding='utf-8') as archivo:
                    json.dump(package_json, archivo, ensure_ascii=False, indent=4)
            input('Proyecto generado con exito. Presione una tecla para continuar...')
            return
    
def msGen():
    mss = os.listdir('./ms')
    otrasOpciones = {}
    otrasOpciones['99'] = 'Crear Nuevo'
    while True:
        opcionMss = util.menuMaker(mss,'MS',otrasOpciones)
        if opcionMss == '0':
            return
        if opcionMss == '99':
            nombre = input('Ingrese el nombre: ')
            envFile = ConfigObj('.env')
            red = envFile['RED']
            auxRutaPysed =  shutil.which("pysed")
            cmds =[]
            cmds.append('cp -r aper_stack/templates/ms-express ms/'+nombre)
            cmds.append('cp  ms/'+nombre+'/.env.example ms/'+nombre+'/.env')
            util.ejecutor(cmds,False,'MS GEN')
            cmds =[]
            cmds.append(auxRutaPysed+" -r \">>>NOMBRE\" "+nombre+"  ./ms/"+nombre+"/.env --write")
            cmds.append(auxRutaPysed+" -r \">>>RED\" "+red+"  ./ms/"+nombre+"/.env --write")
            cmds.append(auxRutaPysed+" -r \">>>IP\" "+util.getIp()+"  ./ms/"+nombre+"/.env --write")
            util.ejecutor(cmds,False,'MS GEN')
            package_json = json.load(open('./ms/'+nombre+'/package.json'))
            package_json["name"] = nombre
            with open('./ms/'+nombre+'/package.json', 'w', encoding='utf-8') as archivo:
                    json.dump(package_json, archivo, ensure_ascii=False, indent=4)
        
            input('Proyecto generado con exito. Presione una tecla para continuar...')
            return

def mainMenu():
    while True:
        arrayOpciones = []
        arrayOpciones.append("Store")
        arrayOpciones.append("Lambda")
        arrayOpciones.append("MS")

        opcion = str.lower(util.menuMaker(arrayOpciones,'ENTORNOS DESARROLLO',[]))
        if opcion == '1':
            entornos()
        elif opcion == '2' :
            lambdaGen()
        elif opcion == '3':
            msGen()
        elif opcion == '0':
            break

if __name__ == "__main__":
    mainMenu()

