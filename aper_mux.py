import libtmux
import subprocess
from signal import signal, SIGINT

try:
    server = libtmux.Server()
    print(server.list_sessions())
except:
    p = subprocess.run(
        ["tmux", "new-session", "-d", "-s",
            'aper-sys'],
        check=True
    )
    #tmux attach-session -t aper-sys
    
    server = libtmux.Server()

session = server.get_by_id('$0')

#window = session.new_window(attach=False, window_name="ha in the bg")
ventana_principal = session.attached_window
ventana_principal.rename_window('APER')

#ventana_principal.cmd('python3 aper.py','-t 0')
pane_menu = ventana_principal.list_panes()[0]#ventana_principal.attached_pane
pane_menu.send_keys('python3 aper.py --tmux 1')

cant_panes = (len(ventana_principal.list_panes()))
if (cant_panes == 1):
    info_pane = ventana_principal.split_window(attach=False,vertical=False,percent=20)
    info_pane.send_keys('python3 info.py')
else:
    info_pane = ventana_principal.list_panes()[1]

info_pane.clear()
server.attach_session('aper-sys')

#pane.select_pane()
#pane = window.split_window()
#
# pane.enter()
#pane.clear()  
#window = session.new_window(attach=False, window_name="test")
#pane = window.split_window(attach=False)