const AWS = require('aws-sdk');
const fs = require('fs');
const path = require('path')
const timer = require('timers');
require('dotenv').config();

const config = {
    region: process.env.AWS_REGION
}

if (process.env.COMMAND_ENV == 'DEV'){
    config.endpoint = process.env.DYNAMODB_ENDPOINT;
    config.apiVersion= 'latest';
    config.credentials= {
        accessKeyId: 'asfasf',
        secretAccessKey: 'asasfsf'
    };
}

const ddb = new AWS.DynamoDB(new AWS.Config(config));

const jsonsInDir = fs.readdirSync('./db').filter(file => path.extname(file) === '.json');

jsonsInDir.forEach(jsonTbl => {
    let rawdata = fs.readFileSync('db/'+jsonTbl);
    let tabla = JSON.parse(rawdata);

    ddb.describeTable({TableName: tabla.TableName},function(err,data){
        if (err){
            ddb.createTable(tabla, function (err, data) {
                if (err) {
                    console.log("Error", err);
                } else {
                    console.log("Table Created", data);
                }
            });
        }
        else{
            ddb.deleteTable({TableName: tabla.TableName}).promise().then(function(){
                timer.promise.setTimeout(1000);
                ddb.createTable(tabla, function (err, data) {
                    if (err) {
                        console.log("Error", err);
                    } else {
                        console.log("Table Created", data);
                    }
                });
            })
        }
    })
})