#!/bin/bash
source .env
source $HOME/.bash_aliases

NOMBRE_COMMAND=$(echo "$NOMBRE" | tr -d '\r')
RED_COMMAND=$(echo "$RED" | tr -d '\r')
PERFIL_AWS=$(echo "$PROFILE_DEV" | tr -d '\r')

#Busqueda de puerto
BASE=3000
INCREMENT=1

port=$BASE
isfree=$(netstat -tapln | grep $port)

while [[ -n "$isfree" ]]; do
  port=$[port+INCREMENT]
  isfree=$(netstat -tapln | grep $port)
done

docker build -t $NOMBRE_COMMAND .
shopt -s expand_aliases
DIR=${PWD}

aper --renew $PERFIL_AWS --output "$DIR/.env"

echo "###########################################################################################"
echo "COMMAND deployando...."
echo "###########################################################################################"

docker run  --network $RED_COMMAND --env-file "$DIR/.env"  $NOMBRE_COMMAND
