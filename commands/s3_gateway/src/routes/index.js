var express = require('express');
var router = express.Router();
var s3 = require('../config/s3');

const fs = require("fs");
const { createCanvas } = require("canvas");

router.get('/:bucket/products/:key', async function(req, res, next) {
  console.log('products/'+req.params.key)
  
  try{
    const img = await s3.getObject({
        Bucket: req.params.bucket,
        Key: 'products/'+req.params.key
    }).promise()
    res.writeHead(200, {'Content-Type': 'image/jpeg'});
    res.write(img.Body, 'binary');
    res.end(null, 'binary');
  }
  catch{
    const width = 300;
    const height = 300;
    const canvas = createCanvas(width, height);

    
    const context = canvas.getContext("2d");

    // Background color
    context.fillStyle = "grey";
    context.fillRect(0, 0, width, height);

    context.font = "bold 15px Arial";
    context.textAlign = "center";

    // Set text color
    context.fillStyle = "black";

    // Write "KindaCode.com"
    context.fillText(req.params.key, width / 2, height / 2);

    const buffer = canvas.toBuffer("image/png");
    
    res.writeHead(200, {'Content-Type': 'image/jpeg'});
    res.write(buffer, 'binary');
    res.end(null, 'binary');
    
    
  }

    // s3.getObject({
    //     Bucket: req.params.bucket,
    //     Key: 'products/'+req.params.key
    //   },function(err,data){
    //     if (err) {
    //       console.log(err)
         
    //     }
    //     res.writeHead(200, {'Content-Type': 'image/jpeg'});
    //     res.write(data.Body, 'binary');
    //     res.end(null, 'binary');
    //   })
});

module.exports = router;