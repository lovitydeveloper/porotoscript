const AWS = require("aws-sdk");
require('dotenv').config()

const config = {
  region: process.env.AWS_REGION,
}

config.apiVersion= 'latest';
config.credentials= {
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    sessionToken: process.env.AWS_SESSION_TOKEN
}


module.exports = new AWS.S3(new AWS.Config(config));