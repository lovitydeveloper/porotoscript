# UPGRADE NOTICE
antes de actualizar a esta version, hacer un docker-compose down de la pila actual, la misma se selcciona despues desde el apartado Docker Stack dentro del menu.

# Documentación
[Link a la Documentación](https://aper.atlassian.net/wiki/spaces/DESA/pages/1140687260/Creaci+n+de+Entornos+Locales)

# Requerimientos
- Docker y docker-compose con permisos de ejecucion para el usuario actual (NO SUDO) para entornos de desarrollo

# Instalacion
- `./instalar.sh`
- la primera vez que se corre `python3 aper.py` hay que elegir la opción CONF y configurar todos los pasos.
- durante la instalación hay que seguir todos los pasos, excepto por el de ICBC que es opcional

## Se entrega en dos sabores, Multiplexado
- `python3 aper_mux.py`

##  o Simple
- `python3 aper.py`


##  Configuración de Xdebug
* Agregar nuestra IP en el archivo .env . Para encontrarla ejecutar el comando ifconfig
* Instalar la extension de chrome para xdebug https://chrome.google.com/webstore/detail/xdebug-helper/eadndfjplgieldjbigjakmdgkmoaaaoc . Habilitarla en la tienda que necesitemos.
* Ejecutar los siguientes comandos para habilitar los puertos en el firewall:
  * sudo ufw allow 9001 & sudo ufw allow 9002
* Para VSCODE
  * Tener instalada la extensión 'PHP Debug' https://marketplace.visualstudio.com/items?itemName=felixfbecker.php-debug .
  * En la barra de herramientas de la izquierda buscar 'Run and Debug'
  * Poner play en la parte superior
  * Poner un breakpoint
* Para PHPSTORM
  * En File -> Settings -> PHP -> Debug , en el apartado Xdebug agregar los puertos 9001 y 9002 al input 'Debug port'
  * Tocar el ícono con un teléfono en la barra de herramientas superior para empezar a escuchar.
  * Poner un breakpoint
  
