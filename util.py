import subprocess
import libtmux
import argparse
import os
from pyfiglet import Figlet
import socket

_singleton=None
_description=""

def ejecutor(cmds,tmux,nombre_instancia):
    if (tmux):
        server = libtmux.Server()
        session = server.get_by_id('$0')
        consola_conx = session.new_window(attach=True, window_name=nombre_instancia)
        consola_conx.attached_pane.send_keys(';'.join(cmds))
    else:
        try:
            for c in cmds:
                print(bcolors.HEADER+c+bcolors.ENDC)
                rta =os.system(c)
                if int(rta) != 0:
                    raise Exception('Error en el comando: '+c)
            
            #p = subprocess.run(cmd, shell=True)
        except KeyboardInterrupt:
            print("Aborted!")
        # except Exception as e:
        #     util.printException(self,e)

def getIp():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(('8.8.8.8', 1))  # connect() for UDP doesn't send packets
    local_ip_address = s.getsockname()[0]
    return local_ip_address    

def returnOrClosePane(tmux):
    if(tmux):
        print('salida Tmux')
        server = libtmux.Server()
        session = server.get_by_id('$0')
        session.kill_window()
    else:    
        return

def checkDocker():
    cmd = subprocess.check_output("docker ps -f name=aper --format '{{ .Names }} - {{ .Status }}'",shell=True)
    if(cmd.decode('ascii') == ''):
        return False
    else:
        return True

def printException(msg):
    print("""
    _.-^^---....,,---,
 _--                  --_  
<                        >)
|                         | 
 \._                   _./  
    ```--. . , ; .--'''       
          | |   |             
       .-=||  | |=-.   
       `-=#$%&%$#=-'   
          | ;  :|     
 _____.,-#%&$@%#&#~,._____
         """)
    print('ALGO EXPLOTÓ:  \n ' + bcolors.FAIL+str(msg)+bcolors.ENDC)
    input('Presione una tecla para continuar...')
    return False

def printWarning(msg):
    print(bcolors.WARNING+str(msg)+bcolors.ENDC)
    input('Presione una tecla para continuar...')

def menuMaker(array,titulo,arrayOtrasOpciones=[],clear=True):
    f = Figlet(font='slant')
    if(clear):
        os.system("clear")
        print(bcolors.OKCYAN+f.renderText(titulo)+bcolors.ENDC)
    else:
        print('\n '+ bcolors.OKCYAN+str(titulo)+bcolors.ENDC)

    i = 1
    for a in array:
        print(str(i)+" : "+a)
        i = i+1

    print(bcolors.HEADER+"###########################"+bcolors.ENDC)
    print(bcolors.OKCYAN+"\n0 : Volver"+bcolors.ENDC)
    for key in arrayOtrasOpciones:
        print(bcolors.OKCYAN+key+" : "+arrayOtrasOpciones[key]+bcolors.ENDC)
    
    choice = input(bcolors.HEADER+"\nSeleccione : "+bcolors.ENDC)
    
    # if choice == '0' or 1 <= int(choice) <= len(array):
    #     return False
    # else:
        # return int(choice)-1
    return choice

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def ArgumentParser(description=None, *arg_l, **arg_d): 
  global _singleton, _description
  if description: 
    if _description: _description+=" & "+description
    else: _description=description
  if _singleton is None: _singleton=argparse.ArgumentParser(description=_description, *arg_l, **arg_d)
  return _singleton