import os
import time
from pyfiglet import Figlet
import subprocess
import json
import libtmux
from dateutil.parser import parse as DTparser
from datetime import timedelta
import datetime
import pytz
import util



f = Figlet(font='slant')

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def traerStatusStack():
    print('############Stack################')
    cmd = subprocess.Popen("docker ps -f name=aper --format '{{ .Names }} - {{ .Status }}'",shell=True).wait()
    print('\n')

def traerStatusDaemon():
    print('############Daemon################')
    
    print('Running')

def traerCantSesiones():
    print('############Sesiones#############')
    server = libtmux.Server()
    session = server.get_by_id('$0')
    print('Cantidad Sesiones: '+str(len(session.list_windows())))
    print('\n')

def traerIp():
    print('############IP####################')
    print(util.getIp())
    print('\n')

def traerTokenExpira():
    print('############Token################')

    if (os.path.isfile('./temp/temp_aper_aws.json') == True):
        ftemp_aws = open ('./temp/temp_aper_aws.json')
        token_aper = json.load(ftemp_aws)
        
        fecha = DTparser(token_aper['Credentials']['Expiration']) - pytz.UTC.localize(datetime.datetime.now()+ timedelta(minutes=200))
        
        print('TOKEN APER Vence en: '+str(fecha).split(':')[0]+' HS, '+str(fecha).split(':')[1]+' Min')
    else:
        print('Token no registrador aún.')
    print('\n')




while True:
    os.system("clear")
    print(f.renderText('INFO'))
   
    traerStatusStack()
    #traerStatusDaemon()
    traerCantSesiones()
    traerTokenExpira()
    traerIp()
    #print ('Estado stack: '+statusStack)
    
    
    time.sleep(15) 