from pathlib import Path
from pyfiglet import Figlet
import os
import json
from configobj import ConfigObj

global parser

global f
global instalar

global aws_dir
global aws_config
global ruta_config_archivo
global config_archivo
global config
global usuarios_ssh
global path_home

global roles
global cuentas

usuarios_ssh = ['ubuntu','www-data','developer','comercial','db-user']

envFile = ConfigObj('./.env')

roles = json.load(open('./aper_stack/conf/aws/roles.json'))
cuentas = json.load(open('./aper_stack/conf/aws/cuentas.json'))
dbs = json.load(open('./aper_stack/conf/dbs/dbs.json'))

#############CUSTOMS
if os.path.isfile('./aper_stack/conf/dbs/dbs_custom.json'):
    dbs_custom = json.load(open ('./aper_stack/conf/dbs/dbs_custom.json'))
    dbs = dbs + dbs_custom 

if os.path.isfile('./aper_stack/conf/aws/cuentas_custom.json'):
    cuentas_custom = json.load(open ('./aper_stack/conf/aws/cuentas_custom.json'))
    cuentas = cuentas + cuentas_custom 
#############/CUSTOMS

path_home = str(Path.home())

aws_dir = str(Path.home())+'/.aws/'
aws_config = ConfigObj(aws_dir+'config')
aws_credentials = ConfigObj(aws_dir+'credentials')

ruta_config_archivo = './aper_stack/conf/aws/config.json'

f = Figlet(font='slant')

instalar = True

def load():
    global aws_config
    global envFile
    aws_config = ConfigObj(aws_dir+'config')
    envFile = ConfigObj('./.env')

if (os.path.isfile(ruta_config_archivo) == True):
    config_archivo = open (ruta_config_archivo)
    config = json.load(config_archivo)
    #Defaults
    config.setdefault('modo_gitlab', 'HTTPS')
    instalar = False
    
    
    
