import os
from pyfiglet import Figlet
import entornos
from aws import renew,menuAws,checkAwsFile
from dbs import menuDbs
import glob
from signal import signal, SIGINT
import argparse
import libtmux
import subprocess
import util
import shutil
import globals
import json
import inquirer

def init():
    globals.parser = util.ArgumentParser(description='Aper Args')
    globals.parser.add_argument("--tmux", help="tmux o shell")
    globals.parser.add_argument("--renew", help="Renew token")
    globals.parser.add_argument("--output", help="Write token")
    
    signal(SIGINT, handler)
    checkAwsFile()
    
def salir():
    if(args.tmux):
        print('salida Tmux')
        server = libtmux.Server()
        session = server.get_by_id('$0')
        session.kill_session()
    else:    
        exit(0)

def handler(signal_received, frame):
    # Handle any cleanup here
    salir()


def dockerStack(tmux):
    if(tmux == False):
        print('######################DOCKER STATUS################################')
        print(subprocess.check_output("docker ps -f name=aper --format '{{ .Names }} - {{ .Status }}'",shell=True).decode("utf-8"))
        print('###################################################################')
    listDirScripts = glob.glob('./docker-compose*.yml')
    listaDir = [listDirScripts[i] for i in range(len(listDirScripts))]

    checks = [
        inquirer.Checkbox('stack', message="Seleccion los componentes con espacio, finalize con un enter.", choices=listaDir,default=globals.envFile["COMPOSE_FILE"].split(':')),
    ]
    answers = inquirer.prompt(checks)
    if (len(answers['stack']) > 0):
        auxUp = ':'
        auxUp = auxUp.join(answers['stack'])
        
        if len(auxUp) > 0:
            globals.envFile["COMPOSE_FILE"] = auxUp
            globals.envFile.write()

            #fix spaces .env
            with open('.env', 'r') as f:
                txt = f.read().replace(' ', '')

            with open('.env', 'w') as f:
                f.write(txt)

            globals.load()
            util.ejecutor(['docker-compose up -d --remove-orphans'],False,"Init Docker")
            input('Stack running...presione una tecla para continuar.')

def main():
    while True:
        arrayOpciones = []
        if (globals.instalar == False):
            arrayOpciones.append("AWS")
            arrayOpciones.append("Entornos Desarrollo")
            arrayOpciones.append("DBS")
            arrayOpciones.append("Docker Stack")
            arrayOpciones.append("Soporte")
        
        arrayOpcionesExtra = {}
        arrayOpcionesExtra["CONF"] = "Configurar"

        opcion = str.lower(util.menuMaker(arrayOpciones,'APER',arrayOpcionesExtra))

        if opcion == '1':
            menuAws(args.tmux)
        elif opcion == '2' :
            entornos.mainMenu()
        elif opcion == '3':
            menuDbs()
        elif opcion == '4':
            dockerStack(args.tmux)
        elif opcion == '5':
            ip = input("Pedile a poroto la ip: ")
            cmds = []
            cmds.append('sudo apt-get install openssh-server')
            cmds.append('id -u soporte &>/dev/null ||sudo  useradd soporte')
            cmds.append('id -u soporte &>/dev/null ||sudo  usermod -aG sudo soporte')
            cmds.append("echo ingrese password usuario soporte:")
            cmds.append("sudo passwd soporte")
            cmds.append('ssh root@'+ip+' -p4501 -R*:2223:localhost:22')
            util.ejecutor(cmds,False,"Soporte")
        elif opcion == '0':
            salir()
        elif opcion == 'conf':
            install()

def install():
    if globals.instalar == True:
        config_template = json.load(open('./aper_stack/conf/aws/config.json.template'))
        globals.config = config_template
        if shutil.which("aws") == None:
            cmds = []
            cmds.append('curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o temp/awscliv2.zip')
            cmds.append('unzip temp/awscliv2.zip -d temp/')
            cmds.append('sudo ./temp/aws/install')
            cmds.append('curl "https://s3.amazonaws.com/session-manager-downloads/plugin/latest/ubuntu_64bit/session-manager-plugin.deb" -o "temp/session-manager-plugin.deb"')
            cmds.append('sudo dpkg -i temp/session-manager-plugin.deb')
            util.ejecutor(cmds,False,'Install')
    apernet_sso_generado=''
    apernet_sts_generado=''
    icbc_generado=''
    while True:
        arrayOpciones = []
        debugActivo = 'ACTIVADO' if globals.envFile["XDEBUG_ENABLE"] =='1' else 'DESACTIVADO'
        
        arrayOpciones.append("Configurar Usuario "+util.bcolors.OKGREEN+globals.config["usuario"]+util.bcolors.ENDC)
        arrayOpciones.append("Metodo Conexion "+util.bcolors.OKGREEN+globals.config["modo"]+util.bcolors.ENDC)
        arrayOpciones.append("Certificado SSH "+util.bcolors.OKGREEN+globals.config["certificado_ssh"]+util.bcolors.ENDC)
        arrayOpciones.append("Configurar Apernet SSO"+util.bcolors.OKGREEN+apernet_sso_generado+util.bcolors.ENDC)
        arrayOpciones.append("Configurar Apernet STS"+util.bcolors.OKGREEN+apernet_sts_generado+util.bcolors.ENDC)
        arrayOpciones.append("Configurar ICBC "+util.bcolors.OKGREEN+icbc_generado+util.bcolors.ENDC)
        arrayOpciones.append("Modo Gitlab "+util.bcolors.OKGREEN+globals.config["modo_gitlab"]+util.bcolors.ENDC)
        arrayOpciones.append("XDEBUG "+util.bcolors.OKGREEN+debugActivo+util.bcolors.ENDC)
        arrayOpciones.append("Netskope certs ")
        arrayOpciones.append("Reset ")
        opcion_menu  = util.menuMaker(arrayOpciones,'Configurar')
            
        if opcion_menu == "1":
            print("Ingrese usuario: ")
            usuario = input()
            # regex = r'\b[A-Za-z0-9._%+-]+@aper.com\b'
            # while not re.fullmatch(regex,correo):
            #     print("Error Vuelva a intentar Ingresar Correo Aper: ")
            #     correo = input()
            globals.config["usuario"] = usuario
        elif opcion_menu == "2":
            print('Metodo Conexion Aws Cli: ')
            print("1 : Host")
            print("2 : Docker")
            opcion = input()
            if opcion == '1':
                cmds = []
                globals.config["modo"]="aws-host"
            elif opcion == '2':
                globals.config["modo"]="aws-docker"
        elif opcion_menu == "3":
            print('Seleccione llave publica SSH: ')
            i=1
            listDirSsh = glob.glob(globals.path_home+'/.ssh/*.pub')
            listaDir = [listDirSsh[i] for i in range(len(listDirSsh))]
            for file_name in listaDir:
                print(str(i)+' : '+file_name)
                i=i+1
            print('99 : Crear Nuevas Llaves')
            choice = input()
            choice = int(choice)
            if choice == 99:
                cmdSsh = "ssh-keygen -N '' -t ed25519 -f ~/.ssh/"+globals.config["usuario"]+" -C "+globals.config["usuario"]
                util.ejecutor([cmdSsh],False,'ssh')
                globals.config['certificado_ssh'] = '~/.ssh/'+globals.config["usuario"]
            else:
                globals.config['certificado_ssh'] = listaDir[choice-1].rstrip('.pub')
        elif opcion_menu == "4":
            arrayRoles = [obj["roles"] for obj in globals.roles if obj["owner"]=='aper-sso'][0]
            c_rol  = util.menuMaker(arrayRoles,'Rol Defecto')
            cuentasAper = [obj for obj in globals.cuentas if obj["owner"]=='aper']

            for p in cuentasAper:
                globals.aws_config["profile "+p["nombre"]+"-"+arrayRoles[int(c_rol)-1]]={}
                globals.aws_config["profile "+p["nombre"]+"-"+arrayRoles[int(c_rol)-1]]['region']='us-east-1'
                globals.aws_config["profile "+p["nombre"]+"-"+arrayRoles[int(c_rol)-1]]["nombre"]=p["nombre"]
                globals.aws_config["profile "+p["nombre"]+"-"+arrayRoles[int(c_rol)-1]]["owner"]=p["owner"]
                globals.aws_config["profile "+p["nombre"]+"-"+arrayRoles[int(c_rol)-1]]["sso_region"]='us-east-1'
                globals.aws_config["profile "+p["nombre"]+"-"+arrayRoles[int(c_rol)-1]]["sso_start_url"]='https://aper.awsapps.com/start'
                globals.aws_config["profile "+p["nombre"]+"-"+arrayRoles[int(c_rol)-1]]["sso_account_id"]=p["account_id"]
                globals.aws_config["profile "+p["nombre"]+"-"+arrayRoles[int(c_rol)-1]]["sso_role_name"]=arrayRoles[int(c_rol)-1]
                globals.aws_config["profile "+p["nombre"]+"-"+arrayRoles[int(c_rol)-1]]["auth_aws"] = 'sso'

            if not os.path.exists(globals.aws_dir):
                os.makedirs(globals.aws_dir)
            globals.aws_config.write()
            globals.load()
            apernet_sso_generado=' GENERADO!'
        elif opcion_menu == "5":
            arrayRoles = [obj["roles"] for obj in globals.roles if obj["owner"]=='aper-sts'][0]
            if len(globals.config['sts']) == 0:
                print("Ingrese AWS KEY ID: ")
                key_id = input()
                
                print("Ingrese AWS KEY SECRET: ")
                key_secret = input()
                
                aper_sts = {}
                aper_sts["nombre"] = 'aper'
                aper_sts["mfa_serial"] = 'arn:aws:iam::515651278341:mfa/'+globals.config["usuario"]
                aper_sts["aws_access_key_id"] = key_id
                aper_sts["aws_secret_access_key"] = key_secret
                aper_sts["region"] ="us-east-1"

                globals.config["sts"].append(aper_sts)

            c_rol  = util.menuMaker(arrayRoles,'Rol Defecto')
            cuentasAper = [obj for obj in globals.cuentas if obj["owner"]=='aper']

            for p in cuentasAper:
                globals.aws_config["profile "+p["nombre"]+"-"+arrayRoles[int(c_rol)-1]]={}
                globals.aws_config["profile "+p["nombre"]+"-"+arrayRoles[int(c_rol)-1]]['region']='us-east-1'
                globals.aws_config["profile "+p["nombre"]+"-"+arrayRoles[int(c_rol)-1]]["nombre"]=p["nombre"]
                globals.aws_config["profile "+p["nombre"]+"-"+arrayRoles[int(c_rol)-1]]["owner"]=p["owner"]
                globals.aws_config["profile "+p["nombre"]+"-"+arrayRoles[int(c_rol)-1]]["rol"] = arrayRoles[int(c_rol)-1]
                globals.aws_config["profile "+p["nombre"]+"-"+arrayRoles[int(c_rol)-1]]["auth_aws"] = 'sts'
                globals.aws_config["profile "+p["nombre"]+"-"+arrayRoles[int(c_rol)-1]]["account_id"] = p["account_id"]

            if not os.path.exists(globals.aws_dir):
                os.makedirs(globals.aws_dir)
            globals.aws_config.write()
            globals.load()
            apernet_sts_generado=' GENERADO!'
        elif opcion_menu == "6":
            arrayRoles = [obj["roles"] for obj in globals.roles if obj["owner"]=='icbc'][0]
            c_rol  = util.menuMaker(arrayRoles,'Rol Defecto')
            cuentasIcbc = [obj for obj in globals.cuentas if obj["owner"]=='icbc']

            for p in cuentasIcbc:
                globals.aws_config["profile "+p["nombre"]+"-"+arrayRoles[int(c_rol)-1]]={}
                globals.aws_config["profile "+p["nombre"]+"-"+arrayRoles[int(c_rol)-1]]['region']='us-east-1'
                globals.aws_config["profile "+p["nombre"]+"-"+arrayRoles[int(c_rol)-1]]["nombre"]=p["nombre"]
                globals.aws_config["profile "+p["nombre"]+"-"+arrayRoles[int(c_rol)-1]]["owner"]=p["owner"]
                globals.aws_config["profile "+p["nombre"]+"-"+arrayRoles[int(c_rol)-1]]["sso_region"]='us-east-1'
                globals.aws_config["profile "+p["nombre"]+"-"+arrayRoles[int(c_rol)-1]]["sso_start_url"]='https://d-906762eeab.awsapps.com/start'
                globals.aws_config["profile "+p["nombre"]+"-"+arrayRoles[int(c_rol)-1]]["sso_account_id"]=p["account_id"]
                globals.aws_config["profile "+p["nombre"]+"-"+arrayRoles[int(c_rol)-1]]["sso_role_name"]=arrayRoles[int(c_rol)-1]
                globals.aws_config["profile "+p["nombre"]+"-"+arrayRoles[int(c_rol)-1]]["auth_aws"]='sso'

            if not os.path.exists(globals.aws_dir):
                os.makedirs(globals.aws_dir)
            globals.aws_config.write()
            globals.load()
            icbc_generado=' GENERADO!'
        elif opcion_menu == "7":
            arrayOpcionesGitlab = []
            arrayOpcionesGitlab.append("HTTPS")
            arrayOpcionesGitlab.append("SSH")
            opcion_gitlab  = util.menuMaker(arrayOpcionesGitlab,'Gitlab')
            globals.config["modo_gitlab"] = arrayOpcionesGitlab[int(opcion_gitlab)-1]
        elif opcion_menu == "8":
            if globals.envFile["XDEBUG_ENABLE"] =='1':
                globals.envFile["XDEBUG_ENABLE"] = '0'
            else:
                globals.envFile["XDEBUG_ENABLE"] = '1'
                globals.envFile["MI_IP"] = util.getIp()
            globals.envFile.write()
            #fix spaces .env
            with open('.env', 'r') as f:
                txt = f.read().replace(' ', '')

            with open('.env', 'w') as f:
                f.write(txt)
                
            globals.load()
            util.ejecutor(['docker-compose -f docker-compose-store.yml down'],False,"Init Docker")
            util.ejecutor(['docker-compose -f docker-compose-store.yml up -d'],False,"Init Docker")
        
        elif opcion_menu == "9":
            cmds = []
            if 'NETSKOPE_INSTALLED' not in globals.envFile or globals.envFile['NETSKOPE_INSTALLED']=='0':
                cmds.append('./aper_stack/netskope_certs/install.sh')
                globals.envFile["NETSKOPE_INSTALLED"] = '1'
                input('Instalado. presione una tecla para continuar')
            else:
                cmds.append('./aper_stack/netskope_certs/uninstall.sh')
                globals.envFile["NETSKOPE_INSTALLED"] = '0'
                input('Desinstalado. presione una tecla para continuar')
            
            util.ejecutor(cmds,False,'Install Netskope Certs')
            
            globals.envFile.write()
            #fix spaces .env
            with open('.env', 'r') as f:
                txt = f.read().replace(' ', '')

            with open('.env', 'w') as f:
                f.write(txt)
                
            globals.load()
            return   

        elif opcion_menu == "10":
            print('esto volvera la configuracion a 0 para volver empezar.')
            c_reset = input('Desea Continuar? (S/N): ')
            if (c_reset.lower() == 's'):
                if os.path.isfile(globals.aws_dir+'config'):
                    os.remove(globals.aws_dir+'config')
                if os.path.isfile(globals.aws_dir+'credentials'):
                    os.remove(globals.aws_dir+'credentials')
                
                globals.config = json.load(open('./aper_stack/conf/aws/config.json.template'))
                
                if os.path.isfile('./aper_stack/conf/aws/credenciales.json'):
                    str_oc = input('Se detecto una configuracion antigua de sts, copiar valores?. S/N')
                    if (str_oc.lower() == 's'):
                        auxCredsOld = json.load(open('./aper_stack/conf/aws/credenciales.json'))
                        if os.path.isfile('./aper_stack/conf/aws/config.json'):
                            auxConfigOld = json.load(open('./aper_stack/conf/aws/config.json'))
                            globals.config["usuario"] = auxConfigOld["usuario"]
                            globals.config["certificado_ssh"] = auxConfigOld["certificado_ssh"]

                        aper_sts = {}
                        aper_sts["nombre"] = 'aper'
                        aper_sts["mfa_serial"] = auxCredsOld[0]['mfa_serial'] #'arn:aws:iam::515651278341:mfa/'mfa_serial+config_old["usuario"]
                        aper_sts["aws_access_key_id"] = auxCredsOld[0]['aws_access_key_id']
                        aper_sts["aws_secret_access_key"] = auxCredsOld[0]['aws_secret_access_key']
                        aper_sts["region"] ="us-east-1"

                        globals.config["sts"].append(aper_sts)
                globals.load()

                if os.path.isfile(globals.ruta_config_archivo):
                    os.remove(globals.ruta_config_archivo)

            else:
                return
            
        else:
            return
              
        with open(globals.ruta_config_archivo, 'w', encoding='utf-8') as archivo:
            json.dump(globals.config, archivo, ensure_ascii=False, indent=4)
        
        if not os.path.exists('./temp'):
                os.makedirs('./temp')
        if not os.path.exists(globals.path_home+'/.ssh/cache'):
            os.makedirs(globals.path_home+'/.ssh/cache')
        
        instalar = False

if __name__ == "__main__":
    init()
    args = globals.parser.parse_args()
    if(args.renew is not None):
        renew(args.renew,args.output)
    else:
        main()
